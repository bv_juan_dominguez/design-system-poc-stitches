import { ComponentType } from "react";

export type WithAs<T> = T & {
	as?: keyof JSX.IntrinsicElements | ComponentType<any>;
};
