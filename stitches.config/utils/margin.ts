// Dependencies
import { PropertyValue } from "@stitches/react";

// Util
export const margin = {
	m: (value: PropertyValue<"margin">) => ({
		margin: value,
	}),

	mt: (value: PropertyValue<"marginTop">) => ({
		marginTop: value,
	}),

	mb: (value: PropertyValue<"marginBottom">) => ({
		marginBottom: value,
	}),

	ml: (value: PropertyValue<"marginLeft">) => ({
		marginLeft: value,
	}),

	ms: (value: PropertyValue<"marginInlineStart">) => ({
		marginInlineStart: value,
	}),

	mr: (value: PropertyValue<"marginRight">) => ({
		marginRight: value,
	}),

	me: (value: PropertyValue<"marginInlineEnd">) => ({
		marginInlineEnd: value,
	}),

	marginX: (value: PropertyValue<"marginInlineStart">) => ({
		marginInlineStart: value,
		marginInlineEnd: value,
	}),

	mx: (value: PropertyValue<"marginInlineStart">) => ({
		marginInlineStart: value,
		marginInlineEnd: value,
	}),

	marginY: (value: PropertyValue<"marginTop">) => ({
		marginTop: value,
		marginBottom: value,
	}),

	my: (value: PropertyValue<"marginTop">) => ({
		marginTop: value,
		marginBottom: value,
	}),
} as const;
