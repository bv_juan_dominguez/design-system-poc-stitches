// Dependencies
import { PropertyValue } from "@stitches/react";

// Util
export const background = {
	bg: (value: PropertyValue<"background">) => ({
		background: value,
	}),

	bgColor: (value: PropertyValue<"backgroundColor">) => ({
		backgroundColor: value,
	}),

	bgImage: (value: PropertyValue<"backgroundImage">) => ({
		backgroundImage: value,
	}),

	bgGradient: (value: PropertyValue<"backgroundImage">) => ({
		backgroundImage: value,
	}),

	bgClip: (value: PropertyValue<"backgroundClip">) => ({
		backgroundClip: value,
	}),

	bgSize: (value: PropertyValue<"backgroundSize">) => ({
		backgroundSize: value,
	}),

	bgPosition: (value: PropertyValue<"backgroundPosition">) => ({
		backgroundPosition: value,
	}),

	bgRepeat: (value: PropertyValue<"backgroundRepeat">) => ({
		backgroundRepeat: value,
	}),

	bgAttachment: (value: PropertyValue<"backgroundAttachment">) => ({
		backgroundAttachment: value,
	}),
} as const;
