// Dependencies
import { PropertyValue } from "@stitches/react";

// Util
export const shadow = {
	shadow: (value: PropertyValue<"boxShadow">) => ({
		boxShadow: value,
	}),
} as const;
