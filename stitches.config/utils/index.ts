// Dependencies
import { margin } from "./margin";
import { padding } from "./padding";
import { background } from "./background";
import { layout } from "./layout";
import { border } from "./border";
import { shadow } from "./shadow";

// Utils
export const utils = {
	...margin,
	...padding,
	...background,
	...layout,
	...border,
	...shadow,
} as const;
