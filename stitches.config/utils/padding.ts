// Dependencies
import { PropertyValue } from "@stitches/react";

// Util
export const padding = {
	p: (value: PropertyValue<"padding">) => ({
		padding: value,
	}),

	pt: (value: PropertyValue<"paddingTop">) => ({
		paddingTop: value,
	}),

	pb: (value: PropertyValue<"paddingBottom">) => ({
		paddingBottom: value,
	}),

	pl: (value: PropertyValue<"paddingLeft">) => ({
		paddingLeft: value,
	}),

	ps: (value: PropertyValue<"paddingInlineStart">) => ({
		paddingInlineStart: value,
	}),

	pr: (value: PropertyValue<"paddingRight">) => ({
		paddingRight: value,
	}),

	pe: (value: PropertyValue<"paddingInlineEnd">) => ({
		paddingInlineEnd: value,
	}),

	paddingX: (value: PropertyValue<"paddingInlineStart">) => ({
		paddingInlineStart: value,
		paddingInlineEnd: value,
	}),

	px: (value: PropertyValue<"paddingInlineStart">) => ({
		paddingInlineStart: value,
		paddingInlineEnd: value,
	}),

	paddingY: (value: PropertyValue<"paddingTop">) => ({
		paddingTop: value,
		paddingBottom: value,
	}),

	py: (value: PropertyValue<"paddingTop">) => ({
		paddingTop: value,
		paddingBottom: value,
	}),
} as const;
