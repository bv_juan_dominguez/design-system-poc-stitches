// Dependencies
import { PropertyValue } from "@stitches/react";

// Util
export const border = {
	borderX: (value: PropertyValue<"borderLeft">) => ({
		borderLeft: value,
		borderRight: value,
	}),

	borderY: (value: PropertyValue<"borderTop">) => ({
		borderTop: value,
		borderBottom: value,
	}),

	borderTopRadius: (value: PropertyValue<"borderTopLeftRadius">) => ({
		borderTopLeftRadius: value,
		borderTopRightRadius: value,
	}),

	borderBottomRadius: (value: PropertyValue<"borderBottomLeftRadius">) => ({
		borderBottomLeftRadius: value,
		borderBottomRightRadius: value,
	}),

	borderRightRadius: (value: PropertyValue<"borderTopRightRadius">) => ({
		borderTopRightRadius: value,
		borderBottomRightRadius: value,
	}),

	borderLeftRadius: (value: PropertyValue<"borderTopLeftRadius">) => ({
		borderTopLeftRadius: value,
		borderTopRightRadius: value,
	}),
} as const;
