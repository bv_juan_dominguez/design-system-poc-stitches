// Dependencies
import { PropertyValue } from "@stitches/react";

// Util
export const layout = {
	d: (value: PropertyValue<"display">) => ({
		display: value,
	}),

	w: (value: PropertyValue<"width">) => ({
		width: value,
	}),

	minW: (value: PropertyValue<"minWidth">) => ({
		minWidth: value,
	}),

	maxW: (value: PropertyValue<"maxWidth">) => ({
		maxWidth: value,
	}),

	h: (value: PropertyValue<"height">) => ({
		height: value,
	}),

	minH: (value: PropertyValue<"minHeight">) => ({
		minHeight: value,
	}),

	maxH: (value: PropertyValue<"maxHeight">) => ({
		maxHeight: value,
	}),

	size: (value: PropertyValue<"height">) => ({
		width: value,
		size: value,
	}),
} as const;
