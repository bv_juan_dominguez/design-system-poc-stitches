export const shadows = {
	small: "0px 4px 12px rgba(18, 25, 84, 0.07)",
	medium: "0px 6px 14px rgba(18, 25, 84, 0.1)",
	large: "0px 10px 16px rgba(18, 25, 84, 0.07)",
	xlarge: "0px 16px 24px rgba(18, 25, 84, 0.07)",
	border: "0 0 0 1px",
} as const;

export const zIndices = {} as const;
