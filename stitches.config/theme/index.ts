// Dependencies
import { colors } from "./colors";
import {
	fonts,
	fontSizes,
	fontWeights,
	letterSpacings,
	lineHeights,
} from "./typography";
import { space } from "./sizes";
import { shadows } from "./elevation";

// Theme
export const theme = {
	// Colors
	colors,

	// Fonts & Texts
	fonts,
	fontSizes,
	fontWeights,
	letterSpacings,
	lineHeights,

	// Sizes & Spacing
	space,

	// Elevation & Shadows
	shadows,
} as const;
