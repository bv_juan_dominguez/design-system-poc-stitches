export const fonts = {
	poppins: "'Poppins', sans-serif",
} as const;

export const fontSizes = {
	xs: ".625rem",
	sm: ".75rem",
	md: ".875rem",
	base: "1rem",
	lg: "1.125rem",
	xl: "1.5rem",
	"2xl": "2rem",
	"3xl": "2.5rem",
	"4xl": "3rem",
	"5xl": "3.5rem",
	"6xl": "4rem",
} as const;

export const fontWeights = {
	regular: 400,
	medium: 500,
	semibold: 600,
	bold: 700,
	extrabold: 900,
} as const;

export const letterSpacings = {
	tighter: "-0.05em",
	tight: "-0.025em",
	normal: "0",
	wide: "0.025em",
	wider: "0.05em",
	widest: "0.1em",
} as const;

export const lineHeights = {
	normal: "normal",
	none: 1,
	sm: "1rem",
	base: "1.5rem",
	lg: "2rem",
	xl: "2.5rem",
	"2xl": "3rem",
	"3xl": "3.5rem",
	"4xl": "4rem",
	"5xl": "4.5rem",
} as const;
