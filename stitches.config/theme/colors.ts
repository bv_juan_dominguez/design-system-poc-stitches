export const colors = {
	// Blue Corp - Primary
	"blue-corp-10": "#F1F6FE", // Details
	"blue-corp-20": "#CFDFFC", // Details
	"blue-corp-30": "#81A9F6",
	"blue-corp-40": "#3A74F8", // Hover
	"blue-corp-50": "#1F61F7", // Primary
	"blue-corp-60": "#204FCC", // Active
	"blue-corp-70": "#153BAA",
	"blue-corp-80": "#0C2B8A",
	"blue-corp-90": "#0C2B8A",

	// Dark Green - Accent
	"dark-green-10": "#DBFAD3",
	"dark-green-20": "#B2F6A9",
	"dark-green-30": "#7CE479",
	"dark-green-40": "#54CA5C", // Hover
	"dark-green-50": "#25A73A", // Accent
	"dark-green-60": "#1B8F38", // Active
	"dark-green-70": "#127836",
	"dark-green-80": "#0B6031",
	"dark-green-90": "#07502E",

	// Neutrals
	white: "#FFFFFF", // Background
	black: "#000000",

	// -- Light Grey
	"light-grey-0": "$white",
	"light-grey-10": "#F5F7FA", // Background
	"light-grey-20": "#EBEFF5", // Disabled
	"light-grey-30": "#DDE3ED", // Borders
	"light-grey-40": "#C8D1E0",
	"light-grey-50": "#AFBACC", // Placeholder

	// -- Dark Grey
	"dark-grey-80": "#58606E", // Párrafos
	"dark-grey-90": "#434A54",
	"dark-grey-100": "#060B25", // Títulos, iconos , background

	// Semantics
	// -- Danger
	"danger-10": "#FEE8DD",
	"danger-20": "#FDCCBB",
	"danger-30": "#F9A999",
	"danger-40": "#F3887D",
	"danger-50": "#EB5454",
	"danger-60": "#CA3D49",
	"danger-70": "#A92A40",
	"danger-80": "#881A37",
	"danger-90": "#701031",

	// - Warning
	"warning-10": "#FEFAD2",
	"warning-20": "#FEF3A6",
	"warning-30": "#FDEA7A",
	"warning-40": "#FBE058",
	"warning-50": "#F9D222",
	"warning-60": "#D6B018",
	"warning-70": "#B38F11",
	"warning-80": "#90700A",
	"warning-90": "#775A06",

	// Sistema
	primary: "$blue-corp-50",
	accent: "$dark-green-50",
	background: "$light-grey-10",
	disabled: "$light-grey-20",
	borders: "$light-grey-30",
	placeholders: "$light-grey-40",
	paragraph: "$dark-grey-80",
	titles: "$dark-grey-100",
	danger: "$danger-50",
	warning: "$warning-50",
} as const;
