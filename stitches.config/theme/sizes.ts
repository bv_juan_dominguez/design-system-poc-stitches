export const space = {
	// Small
	"small-xsmall": ".25rem",
	"small-small": ".5rem",
	"small-medium": "1rem",
	"small-large": "1.5rem",
	"small-xlarge": "2rem",

	// Big
	"big-xsmall": "2.5rem",
	"big-small": "3rem",
	"big-medium": "3.5rem",
	"big-large": "4rem",
	"big-xlarge": "4.5rem",
} as const;

export const sizes = {} as const;
