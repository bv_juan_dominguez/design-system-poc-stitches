// Dependencies
import { createStitches } from "@stitches/react";
import { normalize } from "stitches-normalize-css";
import { theme } from "./theme";
import { media } from "./breakpoints";
import { utils } from "./utils";

// Theme
export const { styled, css, getCssText, globalCss } = createStitches({
	theme,
	media,
	utils,
});

// Global Styles
export const globalStyles = globalCss(...normalize, {
	"*": {
		fontFamily: "$poppins",
	},
	body: {
		position: "relative",
		fontSize: "$base",
		bgColor: "$white",
		p: 0,
		m: 0,
	},
});
