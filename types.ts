// Dependencies
import { NextPage } from "next";
import { AppProps } from "next/app";

// Types
export type Merge<T, U> = T & Omit<U, keyof T>;

export type Layout = (page: React.ReactElement) => React.ReactNode;

export type NextPageWithLayout<T = {}> = NextPage<T> & {
	getLayout?: Layout;
};

export type AppPropsWithLayout = AppProps & {
	Component: NextPageWithLayout;
};
