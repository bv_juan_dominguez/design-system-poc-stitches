// Dependencies
import { useRouter } from "next/router";
import { Heading } from "components/heading";
import { Container, Stack } from "components/layout";
import { Text } from "components/text";
import { PATH_HEADING } from "constants/pathHeading";
import { Button } from "components/button";
import { Icon } from "components/icon";
import { MdArrowBack } from "react-icons/md";
import Link from "next/link";

// Component
const Navbar = (): JSX.Element => {
	// Hooks
	const router = useRouter();

	return (
		<Stack
			as='nav'
			css={{
				w: "100%",
				borderBottom: "1px solid $colors$light-grey-40",
				py: 24,
				position: "fixed",
				bgColor: "$white",
				alignItems: "center",
			}}>
			<Container
				as={Stack}
				css={{ justifyContent: "space-between", alignItems: "center" }}>
				<Stack direction='row' css={{ gap: 24 }}>
					<Heading as='h1' variant='title' size='1'>
						{PATH_HEADING[router.pathname]}
					</Heading>

					<Link href='/' passHref>
						<Button
							size='small'
							variant='text'
							css={{
								fontWeight: "$semibold",
								color: "$primary",
								bgColor: "$blue-corp-10",
							}}
							leftIcon={
								<Icon
									label='Volver'
									as={MdArrowBack}
									css={{ fontSize: "$base" }}
								/>
							}>
							Volver
						</Button>
					</Link>
				</Stack>

				<Stack css={{ gap: 4, alignItems: "center" }}>
					<Text
						as='span'
						variant='paragraph'
						size={4}
						css={{ fontWeight: "$semibold" }}>
						Design System Proof of Concept -
					</Text>

					<Text variant='paragraph' size={4}>
						{"Made with "}
						<Text
							as='span'
							variant='paragraph'
							size={4}
							css={{ fontWeight: "$medium", fontStyle: "italic" }}>
							Stitches
						</Text>
					</Text>
				</Stack>
			</Container>
		</Stack>
	);
};

export default Navbar;
