// Dependencies
import { Box } from "components/layout";
import { ReactElement, ReactNode } from "react";
import { Layout } from "types";
import Navbar from "./navbar";

// Component
const Layout: Layout = (page: ReactElement): ReactNode => {
	return (
		<>
			<Navbar />

			<Box as='main' css={{ pt: 80 }}>
				{page}
			</Box>
		</>
	);
};

export default Layout;
