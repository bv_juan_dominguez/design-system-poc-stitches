// Dependencies
import { Button } from "components/button";
import { Heading } from "components/heading";
import { Icon } from "components/icon";
import { Stack } from "components/layout";
import { Text } from "components/text";
import Link from "next/link";
import { MdArrowForward } from "react-icons/md";

// Component
export interface CardProps {
	title: string;
	description?: string;
	href: string;
}

const Card = ({ title, description, href }: CardProps): JSX.Element => {
	return (
		<Stack
			direction='column'
			css={{
				d: "flex",
				placeItems: "start",
				borderRadius: "1rem",
				shadow: "$border $colors$light-grey-30",
				p: 24,
				gap: 24,
				justifyContent: "space-between",
				transitionProperty: "all",
				transitionTimingFunction: "ease-in-out",
				transitionDuration: ".3s",

				"&:hover": {
					shadow: "$border $colors$light-grey-50",
				},
			}}>
			<Stack direction='column' css={{ gap: 24 }}>
				<Heading variant='title' size={2}>
					{title}
				</Heading>

				<Text
					size={4}
					css={{ fontWeight: "$medium", color: "$dark-grey-90", mb: 8 }}>
					{description}
				</Text>
			</Stack>

			<Link href={href} passHref>
				<Button
					variant='primary'
					size='small'
					rightIcon={<Icon label='Ir' as={MdArrowForward} />}>
					Ver componente
				</Button>
			</Link>
		</Stack>
	);
};

export default Card;
