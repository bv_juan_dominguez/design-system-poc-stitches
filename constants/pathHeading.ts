export const PATH_HEADING: Record<string, string> = {
	"/buttons": "Botones",
	"/headings": "Encabezados",
	"/text": "Párrafos",
	"/spinners": "Spinners",
	"/icons": "Iconos",
};
