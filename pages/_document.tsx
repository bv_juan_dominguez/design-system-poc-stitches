// Dependencies
import React from "react";
import Document, {
	Html,
	Head,
	Main,
	NextScript,
	DocumentContext,
	DocumentInitialProps,
} from "next/document";
import { getCssText } from "stitches.config";

// Document Component
class POCDocument extends Document {
	static async getInitialProps(
		ctx: DocumentContext
	): Promise<DocumentInitialProps> {
		return Document.getInitialProps(ctx);
	}

	render(): JSX.Element {
		return (
			<Html>
				<Head>
					<meta
						name='description'
						content='Esto es un POC de como crear un design system con Stitches'
					/>

					{/* Font Link */}
					<link
						href='https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;0,900;1,400;1,500;1,600;1,700;1,900&display=swap'
						rel='stylesheet'
					/>

					{/* SSR Styles */}
					<style
						id='stitches'
						dangerouslySetInnerHTML={{ __html: getCssText() }}
					/>
				</Head>

				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

// Export
export default POCDocument;
