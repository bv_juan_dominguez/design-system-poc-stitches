// Dependencies
import Head from "next/head";
import { MdArrowForward, MdEmail } from "react-icons/md";
import Stack from "components/layout/stack";
import { Heading } from "components/heading";
import { Button } from "components/button";
import { Icon } from "components/icon";
import { Container, Grid } from "components/layout";
import { NextPageWithLayout } from "types";
import Layout from "modules/layout";

const sizes = ["small", "medium", "large", "x-large"] as const;
const variants = ["primary", "outline", "text"] as const;

// Page
const ButtonsPage: NextPageWithLayout = () => {
	return (
		<>
			<Head>
				<title>Buttons - POC Stitches</title>
			</Head>

			<Container>
				<Stack direction='column' css={{ py: 48, gap: 56 }}>
					<Stack direction='column' css={{ gap: 24 }}>
						<Heading size='2'>Variantes</Heading>

						<Stack css={{ gap: 24 }}>
							{variants.map((variant) => (
								<Button key={`button-${variant}`} variant={variant}>
									Botón
								</Button>
							))}
						</Stack>
					</Stack>

					<Stack direction='column' css={{ gap: 24 }}>
						<Heading size='2'>Tamaños</Heading>

						<Stack css={{ gap: 24, alignItems: "center" }}>
							{sizes.map((size) => (
								<Button key={`button-${size}`} size={size}>
									Botón
								</Button>
							))}
						</Stack>
					</Stack>

					<Stack direction='column' css={{ gap: 24 }}>
						<Heading size='2'>Estado: Disabled</Heading>

						<Stack css={{ gap: 24, alignItems: "center" }}>
							{variants.map((variant) => (
								<Button
									key={`button-${variant}-disabled`}
									variant={variant}
									isDisabled>
									Botón
								</Button>
							))}
						</Stack>
					</Stack>

					<Stack direction='column' css={{ gap: 24 }}>
						<Heading size='2'>Estado: Loading</Heading>

						<Stack css={{ gap: 32 }}>
							{variants.map((variant) => (
								<Stack key={`button-${variant}-loading`} css={{ gap: 24 }}>
									<Button variant={variant} isLoading>
										Botón
									</Button>

									<Button variant={variant} loadingText='Cargando...' isLoading>
										Botón
									</Button>
								</Stack>
							))}
						</Stack>
					</Stack>

					<Stack direction='column' css={{ gap: 24 }}>
						<Heading size='2'>Con iconos</Heading>

						<Stack css={{ gap: 32 }}>
							{variants.map((variant) => (
								<Stack
									key={`icon-${variant}`}
									direction='column'
									css={{ gap: 24, alignItems: "start" }}>
									<Stack direction='row' css={{ gap: 32 }}>
										<Button
											variant={variant}
											leftIcon={
												<Icon
													as={MdEmail}
													label='Ir'
													css={{ fontSize: "$base" }}
												/>
											}>
											Botón
										</Button>

										<Button
											variant={variant}
											rightIcon={
												<Icon
													as={MdArrowForward}
													label='Ir'
													css={{ fontSize: "$base" }}
												/>
											}>
											Botón
										</Button>
									</Stack>
								</Stack>
							))}
						</Stack>
					</Stack>

					<Grid columns={3} css={{ w: "100%", gap: 48 }}></Grid>
				</Stack>
			</Container>
		</>
	);
};

ButtonsPage.getLayout = Layout;

export default ButtonsPage;
