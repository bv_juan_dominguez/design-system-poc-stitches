// Dependencies
import { NextPage } from "next";
import Head from "next/head";
import { Heading } from "components/heading";
import { Spinner } from "components/spinner";
import Stack from "components/layout/stack";
import { Text } from "components/text";

const sizes = ["xs", "sm", "md", "lg", "xl"] as const;
const thicknesses = [1, 2, 3, 4, 5] as const;

// Page
const Spinners: NextPage = () => {
	return (
		<>
			<Head>
				<title>Spinners - POC Stitches </title>
			</Head>

			<Stack direction='column' css={{ px: 120, py: 32, gap: 56 }}>
				<Heading variant='display' size='4'>
					Spinners
				</Heading>

				<Stack css={{ justifyContent: "space-between" }}>
					{thicknesses.map((thickness) => (
						<Stack key={thickness} direction='column' css={{ gap: 56 }}>
							<Heading variant='title' size='2'>
								Thickness {thickness}
							</Heading>

							{sizes.map((size) => (
								<Stack
									key={`${size}-${thickness}`}
									direction='column'
									css={{ gap: 12 }}>
									<Text css={{ fontWeight: "$semibold" }}>Size: {size}</Text>

									<Spinner size={size} thickness={thickness} />
								</Stack>
							))}
						</Stack>
					))}
				</Stack>
			</Stack>
		</>
	);
};

export default Spinners;
