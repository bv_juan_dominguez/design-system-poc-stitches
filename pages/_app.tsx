// Dependencies
import { globalStyles } from "stitches.config";
import { AppPropsWithLayout } from "types";

// App
const MyApp = ({ Component, pageProps }: AppPropsWithLayout) => {
	globalStyles();

	const getLayout = Component?.getLayout ?? ((page) => page);

	return getLayout(<Component {...pageProps} />);
};

export default MyApp;
