// Dependencies
import type { NextPage } from "next";
import Head from "next/head";
import { Heading } from "components/heading";
import { Container, Grid, Stack } from "components/layout";
import { Text } from "components/text";
import Card, { CardProps } from "modules/card";

const availableComponents: Array<CardProps> = [
	{
		title: "Botón",
		description:
			"Componente accesible: 3 variantes, 4 tamaños, 3 estados y soporte para iconos",
		href: "/buttons",
	},
	{
		title: "Textos",
		description: "2 componentes, 2 variantes cada uno, de 4 a 5 tamaños",
		href: "/text",
	},
	{
		title: "Spinners",
		description: "Componente accesible: 4 grosores, 5 tamaños",
		href: "/spinners",
	},
];

// Page
const Home: NextPage = () => {
	return (
		<>
			<Head>
				<title>Design System POC - Stitches</title>
			</Head>

			<Container>
				<Stack direction='column' css={{ py: 48, gap: 56 }}>
					<Stack direction='column' css={{ gap: 12 }}>
						<Heading as='h1' variant='title'>
							Design System Proof of Concept
						</Heading>

						<Text variant='paragraph' size={2}>
							{"Made with "}
							<Text
								size={2}
								css={{
									fontWeight: "$medium",
									fontStyle: "italic",
									d: "inline",

									"& > a": {
										textDecoration: "none",
										color: "black",

										"&:hover": {
											color: "$primary",
											textDecoration: "underline",
										},
									},
								}}>
								<a
									href='https://stitches.dev'
									rel='noopener noreferrer'
									target='_blank'>
									Stitches
								</a>
							</Text>
						</Text>
					</Stack>

					<Grid columns={3} css={{ w: "100%", gap: 48 }}>
						{availableComponents.map((card) => (
							<Card
								key={`${card.title}-${card.href}`}
								title={card.title}
								description={card.description}
								href={card.href}
							/>
						))}
					</Grid>
				</Stack>
			</Container>
		</>
	);
};

export default Home;
