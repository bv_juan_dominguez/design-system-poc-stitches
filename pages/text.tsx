// Dependencies
import { Heading } from "components/heading";
import Grid from "components/layout/grid";
import Stack from "components/layout/stack";
import { Text } from "components/text";
import { NextPage } from "next";
import Head from "next/head";

type HeadingVariantsT = "display" | "title";
type HeadingSizesT = 1 | 2 | 3 | 4 | 5;
type TextVariantsT = "paragraph" | "caption";
type TextSizesT = 1 | 2 | 3 | 4;

const headingVariants: Record<HeadingVariantsT, Array<HeadingSizesT>> = {
	display: [1, 2, 3, 4, 5],
	title: [1, 2, 3, 4],
};
const textVariants: Record<TextVariantsT, Array<TextSizesT>> = {
	paragraph: [1, 2, 3, 4],
	caption: [1, 2],
};
const alignments = ["start", "center", "end"] as const;

// Page
const TextPage: NextPage = () => {
	return (
		<>
			<Head>
				<title>Text - POC Stitches </title>
			</Head>

			<Stack direction='column' css={{ px: 120, py: 32, gap: 56 }}>
				<Heading as='h1' variant='display' size='4'>
					Text Components
				</Heading>

				<Grid columns={3} css={{ gridGap: "40px" }}>
					{(Object.keys(headingVariants) as HeadingVariantsT[]).map((variant) =>
						headingVariants[variant].map((size) =>
							alignments.map((alignment) => (
								<Heading
									key={`${variant}-${size}-${alignment}`}
									variant={variant}
									size={size}
									css={{ textAlign: alignment }}>
									{variant} {size}
								</Heading>
							))
						)
					)}

					{(Object.keys(textVariants) as TextVariantsT[]).map((variant) =>
						textVariants[variant].map((size) =>
							alignments.map((alignment) => (
								<Text
									key={`${variant}-${size}-${alignment}`}
									variant={variant}
									size={size}
									css={{ textAlign: alignment }}>
									{variant} {size}
								</Text>
							))
						)
					)}
				</Grid>
			</Stack>
		</>
	);
};

export default TextPage;
