// Dependencies
import { ComponentProps, ElementRef, forwardRef } from "react";
import { AccessibleIcon } from "@radix-ui/react-accessible-icon";
import { styled } from "stitches.config";
import { WithAs } from "stitches.config/types";

// Base Component
const BaseIcon = styled("svg");
type BaseIconProps = ComponentProps<typeof BaseIcon>;

// Component
export interface IconProps extends WithAs<BaseIconProps> {
	label: string;
}

const Icon = forwardRef<ElementRef<typeof BaseIcon>, IconProps>(
	({ label, ...props }, ref) => {
		return (
			<AccessibleIcon label={label}>
				<BaseIcon ref={ref} {...props} />
			</AccessibleIcon>
		);
	}
);

Icon.displayName = "Icon";

export default Icon;
