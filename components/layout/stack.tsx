// Dependencies
import { styled } from "stitches.config";
import Box from "./box";

// Component
const Stack = styled(Box, {
	d: "flex",

	variants: {
		direction: {
			column: { flexDirection: "column" },
			row: { flexDirection: "row" },
			"column-reverse": { flexDirection: "column-reverse" },
			"row-reverse": { flexDirection: "row-reverse" },
		},
	},

	defaultVariants: {
		direction: "row",
	},
});

Stack.displayName = "Stack";

export default Stack;
