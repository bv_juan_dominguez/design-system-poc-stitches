// Dependencies
import { styled } from "stitches.config";
import Box from "./box";

// Base Component
const Container = styled(Box, {
	paddingInline: "1rem",
	mx: "auto",
	boxSizing: "border-box",
	width: "100%",

	variants: {
		size: {
			sm: { "@small": { maxW: 576 } },
			md: { "@medium": { maxW: 768 } },
			lg: { "@large": { maxW: 992 } },
			xl: { "@xlarge": { maxW: 1200 } },
			fluid: { maxW: "100%" },
		},
	},

	defaultVariants: {
		size: "xl",
	},
});

Container.displayName = "Container";

export default Container;
