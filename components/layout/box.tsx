// Dependencies
import { styled } from "stitches.config";

// Component
const Box = styled("div");

export default Box;
