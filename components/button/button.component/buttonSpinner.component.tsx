// Dependencies
import { Box } from "components/layout";
import { Spinner } from "components/spinner";

// Component
type ButtonSpinnerProps = {
	label?: string;
};

const ButtonSpinner = ({ label }: ButtonSpinnerProps): JSX.Element => {
	return (
		<Box
			css={{
				display: "inline-flex",
				alignItems: "center",
				position: label ? "relative" : "absolute",
				fontSize: "1rem",
				lineHeight: "normal",
				marginInlineEnd: label ? "1rem" : 0,
			}}>
			<Spinner label={label} thickness={2} size='xs' />
		</Box>
	);
};

export default ButtonSpinner;
