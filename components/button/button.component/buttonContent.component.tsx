// Dependencies
import { Box } from "components/layout";
import { ButtonProps } from "./button.component";

// Component
type ButtonContentProps = Pick<
	ButtonProps,
	"leftIcon" | "rightIcon" | "children"
>;

const iconBaseStyles = {
	d: "inline-flex",
	alignSelf: "center",
	flexShrink: 0,
} as const;

const ButtonContent = ({
	leftIcon,
	rightIcon,
	children,
}: ButtonContentProps): JSX.Element => {
	return (
		<>
			{leftIcon && (
				<Box
					as='span'
					css={{
						marginInlineEnd: ".4rem",
						...iconBaseStyles,
					}}>
					{leftIcon}
				</Box>
			)}

			{children}

			{rightIcon && (
				<Box
					as='span'
					css={{
						marginInlineStart: ".4rem",
						...iconBaseStyles,
					}}>
					{rightIcon}
				</Box>
			)}
		</>
	);
};

export default ButtonContent;
