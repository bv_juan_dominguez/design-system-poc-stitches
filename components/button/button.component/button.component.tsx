// Dependencies
import { ComponentProps, ElementRef, forwardRef, ReactElement } from "react";
import { styled } from "stitches.config";
import { WithAs } from "stitches.config/types";
import ButtonContent from "./buttonContent.component";
import ButtonSpinner from "./buttonSpinner.component";
import { buttonStyles } from "../button.styles";

// Base Component
const BaseButton = styled("button", buttonStyles);
type BaseButtonProps = ComponentProps<typeof BaseButton>;

// Component
export interface ButtonProps extends WithAs<BaseButtonProps> {
	loadingText?: string;
	isLoading?: boolean;
	isDisabled?: boolean;
	leftIcon?: ReactElement;
	rightIcon?: ReactElement;
	spinnerPlacement?: "start" | "end";
}

const Button = forwardRef<ElementRef<typeof BaseButton>, ButtonProps>(
	(props, ref) => {
		const {
			isLoading,
			isDisabled,
			loadingText,
			leftIcon,
			rightIcon,
			spinnerPlacement,
			children,
			...rest
		} = props;

		return (
			<BaseButton
				ref={ref}
				role='button'
				disabled={isDisabled || isLoading}
				data-loading={isLoading}
				{...rest}>
				{isLoading && <ButtonSpinner label={loadingText} />}

				{isLoading ? (
					loadingText
				) : (
					<ButtonContent leftIcon={leftIcon} rightIcon={rightIcon}>
						{children}
					</ButtonContent>
				)}
			</BaseButton>
		);
	}
);

Button.displayName = "Button";

export default Button;
