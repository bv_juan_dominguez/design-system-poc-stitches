// Dependencies
import { css } from "stitches.config";

// Styles
export const buttonStyles = css({
	// Base
	appearance: "none",
	outline: "none",
	userSelect: "none",
	w: "auto",
	border: 0,
	borderRadius: "99999px",
	d: "inline-flex",
	alignItems: "center",
	justifyContent: "center",
	position: "relative",
	whiteSpace: "nowrap",
	verticalAlign: "middle",
	fontWeight: "$medium",
	cursor: "pointer",
	transitionProperty: "all",
	transitionTimingFunction: "ease-in-out",
	transitionDuration: ".2s",

	variants: {
		size: {
			small: {
				minWidth: 72,
				h: 32,
				px: 12,
				py: 8,
				fontSize: "$sm",
				lineHeight: "$sm",
			},
			medium: {
				minW: 96,
				h: 40,
				px: 16,
				py: 8,
				fontSize: "$md",
				lineHeight: "$base",
			},
			large: {
				minW: 128,
				h: 48,
				px: 20,
				py: 8,
				fontSize: "$base",
				lineHeight: "$lg",
			},
			"x-large": {
				minW: 160,
				h: 56,
				px: 24,
				py: 8,
				fontSize: "$lg",
				lineHeight: "$xl",
			},
		},

		variant: {
			primary: {
				bgColor: "$primary",
				color: "$white",
				fontWeight: "$bold",

				"&:hover": {
					bgColor: "$blue-corp-40",
				},

				"&:active": {
					bgColor: "$blue-corp-60",
				},

				"&[disabled]": {
					bgColor: "$disabled",
					cursor: "not-allowed",
					color: "$light-grey-50",

					"&:hover": {
						bgColor: "$disabled",
					},
				},
			},
			outline: {
				bgColor: "transparent",
				color: "$dark-grey-100",
				shadow: "$border $colors$light-grey-30",

				"&:hover": {
					bgColor: "$light-grey-10",
				},

				"&:active": {
					bgColor: "$light-grey-20",
				},

				"&[disabled]": {
					bgColor: "transparent",
					shadow: "$border $colors$light-grey-20",
					color: "$light-grey-50",
					cursor: "not-allowed",

					"&:hover": {
						bgColor: "transparent",
					},
				},
			},
			text: {
				color: "$primary",
				bgColor: "transparent",

				"&:hover": {
					bgColor: "$blue-corp-10",
				},

				"&:active": {
					bgColor: "$blue-corp-20",
				},

				"&[disabled]": {
					color: "$light-grey-50",
					cursor: "not-allowed",

					"&:hover": {
						bgColor: "transparent",
					},
				},
			},
		},
	},

	defaultVariants: {
		size: "medium",
		variant: "primary",
	},
});
