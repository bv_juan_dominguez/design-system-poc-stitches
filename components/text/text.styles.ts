// Dependencies
import { css } from "stitches.config";

// Styles
export const textStyles = css({
	// Base
	m: 0,
	p: 0,
	fontFamily: "$poppins",
	fontWeight: "$regular",
	fontStyle: "normal",

	variants: {
		variant: {
			paragraph: {
				fontSize: "$lg",
				lineHeight: "$base",

				"@medium": {
					fontSize: "$xl",
					lineHeight: "$lg",
				},
			},
			caption: { fontSize: "$sm", lineHeight: "$sm" },
		},

		size: {
			1: {
				fontSize: "$lg",
				lineHeight: "$base",

				"@medium": {
					fontSize: "$xl",
					lineHeight: "$lg",
				},
			},

			2: {
				fontSize: "$base",
				lineHeight: "$base",

				"@medium": {
					fontSize: "$lg",
					lineHeight: "$base",
				},
			},

			3: {
				fontSize: "$md",
				lineHeight: "$base",

				"@medium": {
					fontSize: "$base",
					lineHeight: "$base",
				},
			},

			4: {
				fontSize: "$md",
				lineHeight: "$base",
			},
		},
	},

	compoundVariants: [
		// Caption
		{
			variant: "caption",
			size: 1,
			css: {
				fontSize: "$sm",
				lineHeight: "$sm",
			},
		},
		{
			variant: "caption",
			size: 2,
			css: {
				fontSize: "$xs",
				lineHeight: "$sm",
			},
		},
	],

	defaultVariants: {
		size: 1,
		variant: "paragraph",
	},
});
