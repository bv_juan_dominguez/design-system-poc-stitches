// Dependencies
import { ComponentProps } from "react";
import { styled } from "stitches.config";
import { textStyles } from "./text.styles";

// Component
const Text = styled("p", textStyles);
export type TextProps = ComponentProps<typeof Text>;

Text.displayName = "Text";

export default Text;
