// Dependencies
import { ComponentProps, ElementRef, forwardRef } from "react";
import { styled } from "@stitches/react";
import { AccessibleIcon } from "@radix-ui/react-accessible-icon";
import { spinnerStyles } from "./spinner.styles";
import { WithAs } from "stitches.config/types";

// Base Component
const BaseSpinner = styled("div", spinnerStyles);
type BaseSpinnerProps = ComponentProps<typeof BaseSpinner>;

// Component
export interface SpinnerProps extends WithAs<BaseSpinnerProps> {
	label?: string;
}

const Spinner = forwardRef<ElementRef<typeof BaseSpinner>, SpinnerProps>(
	({ label = "Cargando...", ...props }, ref) => {
		return (
			<AccessibleIcon label={label}>
				<BaseSpinner ref={ref} {...props} />
			</AccessibleIcon>
		);
	}
);

Spinner.displayName = "Spinner";

export default Spinner;
