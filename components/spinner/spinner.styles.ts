// Dependencies
import { keyframes } from "@stitches/react";
import { css } from "stitches.config";

// Styles
const spinAnimation = keyframes({
	"0%": { transform: "rotate(0deg)" },
	"100%": { transform: "rotate(360deg)" },
});

export const spinnerStyles = css({
	// Base
	d: "inline-block",
	borderStyle: "solid",
	borderTopColor: "$primary",
	borderRightColor: "$primary",
	borderRadius: 99999,
	borderBottomColor: "transparent",
	borderLeftColor: "transparent",
	animation: `${spinAnimation} 1s linear infinite`,
	height: "3rem",
	width: "3rem",
	margin: 0,

	// Sizes
	$$xs: ".75rem",
	$$sm: "1rem",
	$$md: "1.5rem",
	$$lg: "2rem",
	$$xl: "3rem",

	variants: {
		size: {
			xs: {
				height: "$$xs",
				width: "$$xs",
			},
			sm: {
				height: "$$sm",
				width: "$$sm",
			},
			md: {
				height: "$$md",
				width: "$$md",
			},
			lg: {
				height: "$$lg",
				width: "$$lg",
			},
			xl: {
				height: "$$xl",
				width: "$$xl",
			},
		},
		thickness: {
			1: { borderWidth: 1 },
			2: { borderWidth: 2 },
			3: { borderWidth: 3 },
			4: { borderWidth: 4 },
			5: { borderWidth: 5 },
		},
	},

	defaultVariants: {
		size: "md",
		thickness: 3,
	},
});
