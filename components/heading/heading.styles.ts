// Dependencies
import { css } from "stitches.config";

// Styles
export const headingStyles = css({
	// Base
	fontFamily: "$poppins",
	m: 0,
	p: 0,

	variants: {
		variant: {
			display: {
				fontWeight: "$extrabold",
				fontStyle: "normal",
			},
			title: {
				fontWeight: "$bold",
				letterSpacing: "$tighter",
				fontStyle: "normal",
			},
		},

		size: {
			1: {
				fontSize: "$xl",
				lineHeight: "$lg",
			},

			2: {
				fontSize: "$lg",
				lineHeight: "$base",
			},

			3: {
				fontSize: "$base",
				lineHeight: "$base",
			},

			4: {
				fontSize: "$md",
				lineHeight: "$base",
			},
			5: {
				fontSize: "$sm",
				lineHeight: "$base",
			},
		},
	},

	compoundVariants: [
		// Display
		{
			variant: "display",
			size: 1,
			css: {
				fontSize: "$3xl",
				lineHeight: "$2xl",

				"@medium": {
					fontSize: "$5xl",
					lineHeight: "$4xl",
				},
			},
		},
		{
			variant: "display",
			size: 2,
			css: {
				fontSize: "$2xl",
				lineHeight: "$xl",

				"@medium": {
					fontSize: "$4xl",
					lineHeight: "$3xl",
				},
			},
		},
		{
			variant: "display",
			size: 3,
			css: {
				fontSize: "$xl",
				lineHeight: "$lg",

				"@medium": {
					fontSize: "$3xl",
					lineHeight: "$2xl",
				},
			},
		},
		{
			variant: "display",
			size: 4,
			css: {
				fontSize: "$xl",
				lineHeight: "$lg",

				"@medium": {
					fontSize: "$2xl",
					lineHeight: "$xl",
				},
			},
		},
		{
			variant: "display",
			size: 5,
			css: { fontSize: "$xl", lineHeight: "$lg" },
		},
	],

	defaultVariants: {
		size: "1",
		variant: "title",
	},
});
