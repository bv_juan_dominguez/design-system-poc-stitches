// Dependencies
import { ComponentProps } from "react";
import { styled } from "stitches.config";
import { headingStyles } from "./heading.styles";

// Base Component
const Heading = styled("h2", headingStyles);
export type HeadingProps = ComponentProps<typeof Heading>;

Heading.displayName = "Heading";

export default Heading;
